import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { User } from '../models/user.model';

@Injectable()

export class PreferencesService{
    constructor(
        public storage: Storage
    ){}
    
    create(user: User): Promise<User>{
        return this.storage.set('user', user);
    }

    get(): Promise<User>{
        return this.storage.ready()
        .then((localForage: LocalForage) => {
            return this.storage.get('user');
        });
    }

    delete(): Promise<boolean>{
        return this.storage.remove('user')
          .then(() => true);
    }
}