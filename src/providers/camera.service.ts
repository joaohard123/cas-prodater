import { Injectable } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { ActionSheetController } from 'ionic-angular';

@Injectable()
export class CameraService{
    photos: Array<any> = [];

    constructor(
        public actionSheetCtrl: ActionSheetController,
        private camera : Camera
    ){}
    
      takePicture(sourceType: number): Promise<any>{
        let cameraOptions: CameraOptions = {
          correctOrientation: true,
          quality: 100,
          saveToPhotoAlbum: true,
          sourceType: sourceType
        };
    
        return this.camera.getPicture(cameraOptions)
          .then((fileUri: string) => {
            //this.photos.unshift(fileUri);
          }).catch((error: Error) => console.log('Camera Error: ', error))
      }
}