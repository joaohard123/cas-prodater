import { Injectable } from '@angular/core';
import { ToastController } from 'ionic-angular';

import { User } from '../models/user.model';

import $ from "jquery";
import { NavController, Loading } from 'ionic-angular';
import { HomePage } from '../pages/home/home';
import { Network } from '@ionic-native/network';

@Injectable()
export class SolicitationService {
    private solicitationId = null;
    private auth_user = null;
    private auth_pwd = null;

    public disconnectSubscription;
    private loading: Loading;
    public response_user: User;
    public headers = new Headers({ 'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8' });  

  constructor(
      private toastCtrl: ToastController,
    private network: Network  
    ) {
        this.disconnectSubscription = this.onDisconnect("Falha de Conexão!")
  }

  createSolicitation(
      user: 
      { 
          name: string, 
          first_name: string, 
          username: string, 
          password: string, 
          id: string, 
          org_name: string,
          org_id: string 
        }, 
        navCtrl: NavController, 
        solicitation: any, 
        loading: Loading,
        images: Array<string>
    ): void{

    this.loading = loading;
    this.auth_user = user.username;
    this.auth_pwd = user.password;

    let url = 'http://cas.prodater.teresina.pi.gov.br/webservices/rest.php?version=1.0';
    let json_data = {
        "operation": "core/create",
        "comment": user.username,
        "class": "UserRequest",
        "output_fields": "title, description, service_id, servicesubcategory_id",
        "fields":
        {
           "org_id": "SELECT Organization WHERE name = \'" + user.org_name +  "\'",
           "caller_id":
           {
              "name": user.name,
              "first_name": user.first_name,
           },
           "title": solicitation.title,
           "description": solicitation.description,
           "service_id": solicitation.service,
           "servicesubcategory_id": solicitation.subservice
        }
    }
    
    let data = {
        auth_user: user.username, 
        auth_pwd: user.password, 
        json_data: JSON.stringify(json_data)
    }

    let self = this;

    $.ajax({
        type: "POST",
        url: url,
        dataType: 'json',
		data: { auth_user: data.auth_user, auth_pwd: data.auth_pwd, json_data: data.json_data },
        success: function (data) {
            if(data.code === 0){
                let solicitation_id: string;
                for (var i in data['objects']) {
                    solicitation_id = data['objects'][i].key
                }
                let size = images.length;
                console.log("Tamanho do array: " + size);
                if(size > 0){
                    self.solicitationId = solicitation_id;
                    loading.setContent("Enviando Anexos...");
                    for (let i = 0; i < size; i++){
                        self.uploadImage(user, solicitation_id, navCtrl, loading, images[i]);
                    }
                }else{
                    self.presentToast("Solicitação salva com Sucesso!");
                    navCtrl.pop();
                    loading.dismiss();
                    navCtrl.setRoot(HomePage);
                }
            }else{
                loading.dismiss();
                self.presentToast("Erro ao salvar a Solicitação!");
            }
        },error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(thrownError);
            console.log(xhr.responseText);
          }
    });
  }

  uploadImage(
    user: 
    { 
        name: string, 
        first_name: string, 
        username: string, 
        password: string, 
        id: string, 
        org_name: string,
        org_id: string
      }, 
      solicitation_id: string,
      navCtrl: NavController, 
      loading: Loading,
      image: string
  ){
   let url = 'http://cas.prodater.teresina.pi.gov.br/webservices/rest.php?version=1.0';
   let json_data = {
        "operation": "core/create",
        "comment": user.username,
        "class": "Attachment",
        "output_fields": "*",
        "fields":
        {
           "item_class": "UserRequest",
           "item_id": solicitation_id,
           "item_org_id": "SELECT Organization WHERE name = \'" + user.org_name +  "\'",
           "contents":
            {
                "data": image,
                "filename": "Anexo.jpg",
                "mimetype": "image/jpg"
            }
        }
    }
    
    let data = {
        auth_user: user.username, 
        auth_pwd: user.password, 
        json_data: JSON.stringify(json_data)
    }
    let self = this;
    $.ajax({
        xhr: function() {
 
            function updateProgress(evt) {
                if (evt.lengthComputable) {
                    var percentComplete = (evt.loaded / evt.total) * 100;
                    loading.setContent('Enviando Imagem... ' + Math.trunc(percentComplete) + '%');
                }
            }
            var myXhr = $.ajaxSettings.xhr();
            if(myXhr.upload){
                myXhr.upload.addEventListener('progress',updateProgress, false);
            }
            return myXhr;
        },
        type: "POST",
        url: url,
        dataType: 'json',
        data: { auth_user: data.auth_user, auth_pwd: data.auth_pwd, json_data: data.json_data },
        success: function (data) {
            console.log(data);
            loading.dismiss();
            self.presentToast("Solicitação salva com Sucesso!");
            navCtrl.pop();
            navCtrl.setRoot(HomePage);
            if(data.code != 0){
                loading.dismiss();
                self.presentToast("Solicitação salva! Erro ao enviar anexos!");
            }
        },error: function (xhr, ajaxOptions, thrownError) {
            console.log(xhr.status);
            console.log(thrownError);
            console.log(xhr.responseText);
            loading.dismiss();
            self.presentToast("Solicitação salva! Erro ao enviar anexos!");
            loading.dismiss();
          }
    });
  }

  presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    });
  
    toast.present();
  }

  private onDisconnect(message: string){
    this.network.onDisconnect().subscribe(data => {
        this.loading.dismissAll();
        if(this.solicitationId){
            this.toastCtrl.create({
                message: "Erro ao salvar anexos!",
                duration: 3000
            }).present()
        }else{
            this.toastCtrl.create({
                message: message,
                duration: 3000
            }).present()
        }
    }, error => console.log(error))
  }

  private deleteSolicitation() {

    if(this.auth_user && this.auth_pwd && this.solicitationId){
        let url = 'http://cas.prodater.teresina.pi.gov.br/webservices/rest.php?version=1.0';
        let json_data = {
            "operation": "core/delete",
            "comment": this.auth_user,
            "class": "UserRequest",
            "key": this.solicitationId,
            "simulate": false
        }
        
        let data = {
            auth_user: this.auth_user, 
            auth_pwd: this.auth_pwd, 
            json_data: JSON.stringify(json_data)
        }
        let self = this;

        $.ajax({
            type: "POST",
            url: url,
            dataType: 'json',
            data: { auth_user: data.auth_user, auth_pwd: data.auth_pwd, json_data: data.json_data },
            success: function (data) {
                if(data.code === 0){
                    self.presentToast("Alterações desfeitas");
                }else{
                    self.loading.dismissAll;
                    self.presentToast("Erro ao desfazer a Solicitação!");
                }
            },error: function (xhr, ajaxOptions, thrownError) {
                console.log(xhr.status);
                console.log(thrownError);
                console.log(xhr.responseText);
              }
        });
    }
  }

  ionViewDidLeave(){
    this.disconnectSubscription.unsubscribe();
  }
} 
