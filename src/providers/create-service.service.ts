import { Injectable } from '@angular/core';
import { SqliteHelperService } from './sqlite-helper.service';
import { Service } from '../models/service.model';
import { SubService } from '../models/subservice.model';

import $ from "jquery";
import { ServiceSubService } from './service-sub.service';
import { Loading, LoadingController } from 'ionic-angular';

@Injectable()

export class CreateServiceService{

    constructor(
        public sqlteHelperService: SqliteHelperService,
        public serviceSubService: ServiceSubService,
        public loadingCtrl: LoadingController
    ){}
    
    createService(user: { username: string, password: string, id: string, org_id: string }, loading: Loading): boolean{
        let saved: boolean;
        let url = 'http://cas.prodater.teresina.pi.gov.br/webservices/rest.php?version=1.0';
        let json_data = {
            "operation": "core/get",
            "class": "Service",
            "output_fields": "id, name",
            //"key": "SELECT Service WHERE org_id = " + user.org_id
            "key": "SELECT Service WHERE org_id = 2"
        }
        
        let data = {
            auth_user: user.username, 
            auth_pwd: user.password, 
            json_data: JSON.stringify(json_data)
        }
    
        let self = this;
    
        $.ajax({
            type: "POST",
            url: url,
            dataType: 'json',
            data: { auth_user: data.auth_user, auth_pwd: data.auth_pwd, json_data: data.json_data },
            success: function (data) {
                console.log(data);
                self.serviceSubService.existsSubservices()
                .then((haveRows: boolean) => {
                    if(!haveRows){
                      loading.setContent("Buscando Subserviços!");
                      self.createSubService(user, loading);
                    }else{
                        loading.dismiss();
                    }
                })

                saved = true;
                let service_key: string;
                for (var i in data['objects']) {
                    //console.log(i);
                    service_key = i;
                    let service: Service = {
                        _id: data['objects'][i].fields.id,
                        name: data['objects'][i].fields.name
                    }
                    self.serviceSubService.createService(service)
                    .then((service: Service) => {
                        console.log("Services Criados No banco de dados")
                    })
                }

            }
        });
        return saved;
    }

    createSubService(user: { username: string, password: string, id: string, org_id: string }, loading: Loading): boolean{
        let saved: boolean;
        let url = 'http://cas.prodater.teresina.pi.gov.br/webservices/rest.php?version=1.0';
        let json_data = {
            "operation": "core/get",
            "class": "ServiceSubcategory",
            "output_fields": "id,name,description,service_id,service_name,request_type",
            //"key": "SELECT ServiceSubcategory WHERE service_org_id = " + user.org_id
            "key": "SELECT ServiceSubcategory WHERE service_org_id = 2"
        }
        
        let data = {
            auth_user: user.username, 
            auth_pwd: user.password, 
            json_data: JSON.stringify(json_data)
        }
    
        let self = this;
    
        $.ajax({
            type: "POST",
            url: url,
            dataType: 'json',
            data: { auth_user: data.auth_user, auth_pwd: data.auth_pwd, json_data: data.json_data },
            success: function (data) {
                console.log(data);
                saved = true;
                let sub_service_key: string;
                for (var i in data['objects']) {
                    sub_service_key = i;
                    let sub_service: SubService = {
                        _id: data['objects'][i].fields.id,
                        name: data['objects'][i].fields.name,
                        request_type: data['objects'][i].fields.request_type,
                        service_id: data['objects'][i].fields.service_id,
                        service_name: data['objects'][i].fields.service_name,
                    }
                    self.serviceSubService.createSubservice(sub_service)
                    .then((service: SubService) => {
                        console.log("SubServiço Criado Com Sucesso")
                    })
                }
                loading.dismiss();
            }
        });
        return saved;
    }
}