import { Injectable } from '@angular/core';
import { SqliteHelperService } from './sqlite-helper.service';
import { SQLiteObject } from '@ionic-native/sqlite';
import { Service } from '../models/service.model';
import { SubService } from '../models/subservice.model';

@Injectable()

export class ServiceSubService{
    private db: SQLiteObject;
    private isFirstCall: boolean = true;

    private sql_table_subservice = 'CREATE TABLE IF NOT EXISTS subservice (id INTEGER PRIMARY KEY AUTOINCREMENT, _id TEXT, name TEXT, request_type TEXT, service_id TEXT, service_name TEXT)';
    private sql_insert_into_service = 'INSERT INTO service (_id, name) VALUES (?, ?)';
    private sql_insert_into_subservice = 'INSERT INTO subservice (_id, name, request_type, service_id, service_name) VALUES (?, ?, ?, ?, ?)';

    constructor(
        public sqlteHelperService: SqliteHelperService
    ){}
    
    private getDb(): Promise<SQLiteObject>{
        if(this.isFirstCall){
          this.isFirstCall = false;
          return this.sqlteHelperService.getDb('dynamicbox.db')
            .then((db: SQLiteObject) => {
              this.db = db;
              this.db.executeSql('CREATE TABLE IF NOT EXISTS service (id INTEGER PRIMARY KEY AUTOINCREMENT, _id TEXT, name TEXT)', {})
                .then((success => console.log('Service table created sucessfuly!', success)))
                .catch((error: Error) => console.log(' Error creating service table!', error));
              this.db.executeSql(this.sql_table_subservice, {})
                .then((success => console.log('Sub Service table created sucessfuly!', success)))
                .catch((error: Error) => console.log(' Error creating service table!', error));
              return this.db;
            })
        }
        return this.sqlteHelperService.getDb();
    }

    createService(service: Service): Promise<Service> {
        return <Promise<Service>> this.db.executeSql(
          this.sql_insert_into_service, 
          [
            service._id, 
            service.name
          ]
        )
          .then(resultSet => {
            service.id = resultSet.insertId;
            return service;
          }).catch((error: Error) => console.log('Erro creating', error));
    }

    createSubservice(subService: SubService): Promise<SubService> {
        return <Promise<SubService>> this.db.executeSql(
          this.sql_insert_into_subservice, 
          [
            subService._id, 
            subService.name,
            subService.request_type,
            subService.service_id,
            subService.service_name
          ]
        )
          .then(resultSet => {
            subService.id = resultSet.insertId;
            return subService;
          }).catch((error: Error) => console.log('Erro creating', error));
    }

    getServices(): Promise<Service[]>{
      return this.getDb()
      .then((db: SQLiteObject) => {
        return <Promise<Service[]>>this.db.executeSql('SELECT * FROM service', {})
          .then(resultSet => {
            let list: Service[] = [];
            for(let i = 0; i < resultSet.rows.length; i++){
              list.push(resultSet.rows.item(i));
            }
            return list;
          }).catch((error: Error) => console.log('Error executing method getAll', error));
      });
    }

    getSubservices(condition?: string): Promise<SubService[]>{
      return this.getDb()
      .then((db: SQLiteObject) => {
        return <Promise<SubService[]>>this.db.executeSql('SELECT * FROM subservice WHERE service_id=' + condition, {})
          .then(resultSet => {
            let list: SubService[] = [];
            for(let i = 0; i < resultSet.rows.length; i++){
              list.push(resultSet.rows.item(i));
            }
            return list;
          }).catch((error: Error) => console.log('Error executing method getAll', error));
      });
    }

    existsServices(): Promise<boolean>{
      return this.getDb()
      .then((db: SQLiteObject) => {
        return <Promise<boolean>>this.db.executeSql('SELECT * FROM service', {})
          .then(resultSet => {
            let haveRows: boolean;
            if(resultSet.rows.length > 0){
              haveRows = true;
            }else{
              haveRows = false;
            }
            return haveRows;
          }).catch((error: Error) => console.log('Error executing method getAll', error));
      });
    }

    existsSubservices(): Promise<boolean>{
      return this.getDb()
      .then((db: SQLiteObject) => {
        return <Promise<boolean>>this.db.executeSql('SELECT * FROM subservice',{})
          .then(resultSet => {
            let haveRows: boolean;
            if(resultSet.rows.length > 0){
              haveRows = true;
            }else{
              haveRows = false;
            }
            return haveRows;
          }).catch((error: Error) => console.log('Error executing method getAll', error));
      });
    }

    deleteServices(): void{
      this.getDb()
      .then((db: SQLiteObject) => {
        this.db.executeSql('DELETE FROM service',{})
          .then(resultSet => {
            console.log('Todos Os Serviços Foram Apagados')
          }).catch((error: Error) => console.log('Error executing method getAll', error));
      });
    }

    deleteSubServices(): void{
      this.getDb()
      .then((db: SQLiteObject) => {
        this.db.executeSql('DELETE FROM subservice',{})
          .then(resultSet => {
            console.log('Todos Os SubServiços Foram Apagados')
          }).catch((error: Error) => console.log('Error executing method getAll', error));
      });
    }
}