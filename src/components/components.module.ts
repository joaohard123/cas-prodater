import { NgModule } from '@angular/core';
import { AllSolicitationsComponent } from './all-solicitations/all-solicitations';
import { IonicModule } from 'ionic-angular';

@NgModule({
	declarations: [AllSolicitationsComponent],
	imports: [IonicModule],
	exports: [AllSolicitationsComponent]
})
export class ComponentsModule {}
