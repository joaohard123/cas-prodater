export class User{
    public id: string;
    public auth_user: string;
    public auth_pwd: string;

    public email: string;
    public employee_number:string;
    public finalclass: string;
    public first_name: string;
    public friendlyname: string;
    public _function: string;
    public location_id: string;
    public location_id_friendlyname: string;
    public location_name: string;
    public manager_id: string;
    public manager_id_friendlyname: string;
    public manager_name: string;
    public mobile_phone: string;
    public name: string;
    public notify: string;
    public org_id: string;
    public org_id_friendlyname: string;
    public org_name: string;
    public phone: string;
    public status: string;
    constructor(
        
    ){
    }
}