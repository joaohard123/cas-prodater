export class SubService{
    public id?:number;

    constructor(
        public _id: string,
        public name: string,
        public request_type: string,
        public service_id: string,
        public service_name: string
    ){}
}