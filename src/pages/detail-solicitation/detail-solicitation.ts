import { Component } from '@angular/core';
import { NavController, NavParams, Loading, LoadingController, ModalController } from 'ionic-angular';
import { ToastController } from 'ionic-angular';
import { Solicitation } from '../../models/solicitation.model';
import { PreferencesService } from '../../providers/preferences.service';
import { User } from '../../models/user.model';
import { Attchatment } from '../../models/attachment.model';

import $ from "jquery";
import { ModalContentPage } from '../modal-content/modal-content';
import { ModalCommentPage } from '../modal-comment/modal-comment';
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';

@Component({
  selector: 'page-detail-solicitation',
  templateUrl: 'detail-solicitation.html',
})
export class DetailSolicitationPage {
  solicitation: Solicitation;
  user: User;
  loading: Loading;
  attachments: Attchatment[] = [];
  aplicationType: string;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public loadingCtrl: LoadingController,
    public preferences: PreferencesService,
    public modalCtrl: ModalController,
    private file: File,
    private fileOpener: FileOpener,
    public toastCtrl: ToastController
  ) {
    this.solicitation = this.navParams.get('solicitation');
  }

  ionViewDidLoad() {
    this.loading = this.showLoading();
    this.preferences.get()
      .then((user: User) => {
        this.user = user;
        this.getAttachments({
          username: user.auth_user,
          password: user.auth_pwd,
          id: user.id,
          org_id: user.org_id
        })
      })
  }

  private showLoading(): Loading{
    let loading: Loading = this.loadingCtrl.create({
      content: 'Buscando Anexos...'
    });

    loading.present();

    return loading;
  }

  getAttachments(user: { username: string, password: string, id: string, org_id: string }){
    let url = 'http://cas.prodater.teresina.pi.gov.br/webservices/rest.php?version=1.0';
    let json_data = {
        "operation": "core/get",
        "class": "Attachment",
        "output_fields": "*",
        "key": "SELECT Attachment WHERE item_id LIKE '%"+ this.solicitation.id +"'"
    }
    
    let data = {
        auth_user: user.username, 
        auth_pwd: user.password, 
        json_data: JSON.stringify(json_data)
    }

    let self = this;

    $.ajax({
        type: "POST",
        url: url,
        dataType: 'json',
		    data: { auth_user: data.auth_user, auth_pwd: data.auth_pwd, json_data: data.json_data },
        success: function (data) {
          for (var i in data['objects']) {
            let name = data['objects'][i].fields.contents.filename;
            let attachment: Attchatment = {
              id: data['objects'][i].key,
              image: data['objects'][i].fields.contents.data,
              filename: name,
              mimeType: name.slice((name.length - 4), name.length)
            }
            console.log(attachment);
            self.attachments.unshift(attachment);
          }

          self.loading.dismiss();
        }
    });
  }

  openDocument(attachment: Attchatment){
    let archiveUrl = this.returnBase64(attachment);
    
    fetch(archiveUrl,
    {
      method: "GET"
    }).then(res => res.blob()).then(blob => {
      this.file.writeFile(this.file.externalApplicationStorageDirectory, attachment.filename, blob, { replace: true })
      .then(res => {
        this.fileOpener.open(
          res.toInternalURL(),
          this.aplicationType
        ).then((res) => {

        }).catch(err => {
          console.log('open error')
        });
      }).catch(err => {
            console.log('save error')     
      });
      }).catch(err => {
        this.presentToast("Desculpe! Não é possível abrir este tipo de arquivo pelo celular.");
      });
  }

  returnBase64(attachment: Attchatment): string{
    let archive: string;
    let mimetype = attachment.filename.slice((attachment.filename.length - 4), attachment.filename.length);
    console.log(mimetype);

    if(mimetype === '.PDF' || mimetype === '.pdf'){
      archive = "data:application/pdf;base64," + attachment.image;    
      this.aplicationType = 'application/pdf';
    }else if(mimetype === '.JPG' || mimetype === '.jpg'){
      archive = "data:image/jpg;base64," + attachment.image;
      this.aplicationType = 'image/jpeg';
    }else if(mimetype === '.PNG' || mimetype === '.png'){
      archive = "data:image/png;base64," + attachment.image;
      this.aplicationType = 'image/png';
    }
    return archive;
  }

  openAttachmentModal(){
    let modal = this.modalCtrl.create(ModalContentPage, { solicitation: this.solicitation });
    modal.present();
  }

  openCommentModal(){
    let modal = this.modalCtrl.create(ModalCommentPage, { solicitation: this.solicitation });
    modal.present();
  }

  presentToast(message: string) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000
    });
    toast.present();
  }
}
