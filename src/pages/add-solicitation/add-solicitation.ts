import { Component } from '@angular/core';
import { 
  NavController, 
  NavParams, 
  Loading, 
  LoadingController, 
  ActionSheetController, 
  ToastController } from 'ionic-angular';
import { Service } from '../../models/service.model';
import { ServiceSubService } from '../../providers/service-sub.service';
import { SubService } from '../../models/subservice.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SolicitationService } from '../../providers/solicitation.service';
import { User } from '../../models/user.model';
import { PreferencesService } from '../../providers/preferences.service';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { Base64 } from '@ionic-native/base64';
import { FilePath } from '@ionic-native/file-path';
import { File, Entry } from '@ionic-native/file';
import { BackgroundMode } from '@ionic-native/background-mode';

@Component({
  selector: 'page-add-solicitation',
  templateUrl: 'add-solicitation.html',
})
export class AddSolicitationPage {
  form: FormGroup;
  solicitation: {
    title: string,
    description: string,
    service: number,
    subservice: number
  }
  user: User;

  services: Service[] = [];
  subServices: SubService[] = [];

  images: Array<string> = [];
  photoUri: Entry;
  images_b64: Array<string> = [];
  grid: Array<Array<string>>;

  constructor(
    public formBuilder: FormBuilder,
    public navCtrl: NavController, 
    public navParams: NavParams,
    public serviceSubService: ServiceSubService,
    public loadingCtrl: LoadingController,
    public solicitationService: SolicitationService,
    public preferences: PreferencesService,
    public actionSheetCtrl: ActionSheetController,
    public filePath: FilePath,
    public file: File,
    private camera : Camera,
    private photoViewer: PhotoViewer,
    private base64: Base64,
    private toastCtrl: ToastController,
    private backgroundMode: BackgroundMode
  ) {
    this.grid = Array(Math.ceil(this.images.length/2)); //MATHS!

    this.form = this.formBuilder.group({
      title: ['', [ Validators.required, Validators.minLength(3) ]],
      service: ['', [ Validators.required ]],
      subService: ['', [ Validators.required ]],
      description: ['', [ Validators.required, Validators.minLength(3) ]]
    });
  }

  ionViewDidLoad() {
    let loading: Loading = this.showLoading("Carregando serviços!")
    this.serviceSubService.getServices()
      .then((list: Service[]) => {
        loading.dismiss();
        this.services = list;
      })
  }

  onSave(): void{
    

    if(this.form.invalid){
      this.presentToast("Preencha os campos corretamente!");
    }else{
      let loading: Loading = this.showLoading();
      let title = this.form.value.title;
      let description = this.form.value.description;
      let service = this.form.value.service.id;
      let subservice = this.form.value.subService._id
  
      this.preferences.get()
      .then((user: User) => {
        this.user = user;
        this.solicitation = { title, description, service, subservice }
        this.solicitationService.createSolicitation(
          {
            name: user.name,
            first_name: user.first_name,
            username: user.auth_user,
            password: user.auth_pwd,
            id: user.id,
            org_name: user.org_name,
            org_id: user.org_id
          }, 
          this.navCtrl, 
          this.solicitation, 
          loading,
          this.images_b64
        )
      })
    }
  }

  onAddAttachment(): void{
    this.presentActionSheet();
  }

  onSelectChange(selectedValue: any): void{
    let loading: Loading = this.showLoading('Buscando Suberviços');
    this.serviceSubService.getSubservices(selectedValue._id)
      .then((subService: SubService[]) => {
        this.subServices = subService;
        loading.dismiss();
      })
  }

  showFullScreenPhoto(fileUri: string){
    this.photoViewer.show(fileUri, '', {share: false});
  }

  private showLoading(title?:string): Loading{
    let loading = this.loadingCtrl.create({
      content: title || 'Cadastrando Solicitação...'
    });

    loading.present();

    return loading;
  }

  iterate() {
      let rowNum = 0; //counter to iterate over the rows in the grid
      for (let i = 0; i < this.images.length; i+=2) { //iterate images
        this.grid[rowNum] = Array(2); //declare two elements per row
    
        if (this.images[i]) { //check file URI exists
          this.grid[rowNum][0] = this.images[i] //insert image
        }
    
        if (this.images[i+1]) { //repeat for the second image
          this.grid[rowNum][1] = this.images[i+1]
        }
    
        rowNum++; //go on to the next row
      }
    }

    removeImageFromList(image: string){
      let position = this.images.indexOf(image);
      this.images_b64.splice(position, 1);
      this.images.splice(position, 1);
    }

    presentActionSheet() {
      let actionSheet = this.actionSheetCtrl.create({
        title: 'Anexar arquivo',
        buttons: [
          {
            text: 'Camera',
            handler: () => {
              this.backgroundMode.enable();
              this.takePicture(this.camera.PictureSourceType.CAMERA);
            }
          },{
            text: 'Memoria Interna',
            handler: () => {
              this.backgroundMode.enable();
              this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
            }
          },{
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
              console.log('Cancel clicked');
            }
          }
        ]
      });
      actionSheet.present();
    }

    takePicture(sourceType: number){
      let cameraOptions: CameraOptions = {
        correctOrientation: true,
        quality: 50,
        saveToPhotoAlbum: true,
        sourceType: sourceType,
        encodingType: this.camera.EncodingType.JPEG,
        mediaType: this.camera.MediaType.PICTURE
      };
      
      this.camera.getPicture(cameraOptions)
        .then((fileUri: string) => {
          this.backgroundMode.disable();
          this.images.unshift(fileUri);      
          this.iterate();
          this.convertToBase64(fileUri);
        }).catch((error: Error) => { 
          this.backgroundMode.disable();
          console.log('Camera Error: ', error)
        })
    }

    convertToBase64(filePath: string): void{
      this.base64.encodeFile(filePath)
      .then((base64File: string) => {
        let string = base64File.replace("data:image/*;charset=utf-8;base64,"
        , "");
        this.images_b64.unshift(string);
      }).catch((error: Error) => console.log('Camera Error: ', error))
    }

    presentToast(message: string) {
      let toast = this.toastCtrl.create({
        message: message,
        duration: 3000,
        position: 'bottom'
      });
    
      toast.present();
    }
}