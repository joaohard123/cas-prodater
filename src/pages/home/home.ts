import { Component, Input } from '@angular/core';
import { NavController, Loading, LoadingController, PopoverController, Platform } from 'ionic-angular';
import { SigninPage } from '../signin/signin';
import { PreferencesService } from '../../providers/preferences.service';
import { User } from '../../models/user.model';

import $ from "jquery";
import { Solicitation } from '../../models/solicitation.model';
import { AddSolicitationPage } from '../add-solicitation/add-solicitation';
import { CreateServiceService } from '../../providers/create-service.service';
import { ServiceSubService } from '../../providers/service-sub.service';
import { DetailSolicitationPage } from '../detail-solicitation/detail-solicitation';
import { PopoverPage } from '../popover/popover';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  @Input() searchTerm: string;
  
  showSearchBar: boolean = false;
  view: string = 'Novas';
  user: User;
  loading: Loading;
  solicitations: Solicitation[] = [];
  new_solicitations: Solicitation[] = []; //SOLICITAÇÕES NOVAS
  solved_solicitations: Solicitation[] = []; //SOLICITAÇÕES RESOLVIDAS
  searchArray: Solicitation[] = [];

  haveNewSolicitation: boolean = false;
  
  constructor(
    public navCtrl: NavController, 
    public preferences: PreferencesService, 
    public loadingCtrl: LoadingController,
    public createService: CreateServiceService,
    public serviceSubService: ServiceSubService,
    private popoverCtrl: PopoverController,
    public platform: Platform
  ) {
    
  }

  swipeEvent(event){
    if (event.direction == 2) {
      this.view = 'Novas';
    }else{
      this.view = 'Resolvidas';
    }
  }

  ionViewDidLoad(){
  this.loading = this.showLoading();
  this.preferences.get()
    .then((user: User) => {
      this.user = user;
      this.getSolicitations({
        username: user.auth_user,
        password: user.auth_pwd,
        id: user.id,
        org_id: user.org_id
      });
    })
  }

  doRefresh(refresher) {
    this.preferences.get()
    .then((user: User) => {
      this.user = user;
      this.getSolicitations({
        username: user.auth_user,
        password: user.auth_pwd,
        id: user.id,
        org_id: user.org_id
      });
    })

    setTimeout(() => {
      console.log('Async operation has ended');
      refresher.complete();
    }, 2000);
  }

  onClick(): void{
    this.navCtrl.push(SigninPage)
  }

  getSolicitations(user: { username: string, password: string, id: string, org_id: string }): void{
    let url = 'http://cas.prodater.teresina.pi.gov.br/webservices/rest.php?version=1.0';
    let json_data = {
        "operation": "core/get",
        "class": "UserRequest",
        "output_fields": "ref,org_id,org_name,caller_id,caller_name,title,description,start_date,end_date,last_update,close_date,status,request_type,origin,approver_id,approver_email, service_id,service_name,servicesubcategory_id,servicesubcategory_name,caller_id_friendlyname",
        "key": "SELECT UserRequest WHERE caller_id = \'" + user.id +  "\'"
    }
    
    let data = {
        auth_user: user.username, 
        auth_pwd: user.password, 
        json_data: JSON.stringify(json_data)
    }

    let self = this;

    $.ajax({
        type: "POST",
        url: url,
        dataType: 'json',
		    data: { auth_user: data.auth_user, auth_pwd: data.auth_pwd, json_data: data.json_data },
        success: function (data) {
          console.log(data)
          //self.serviceSubService.deleteServices();
          //self.serviceSubService.deleteSubServices();
          /*self.serviceSubService.existsServices()
          .then((haveRows: boolean) => {
              if(!haveRows){
                  self.createService.createService(user);
              }
          })
          self.serviceSubService.existsSubservices()
          .then((haveRows: boolean) => {
              if(!haveRows){
                self.createService.createSubService(user);
              }
          })*/
          
            self.solicitations = [];

            for (var i in data['objects']) {
              let solicitation: Solicitation = {
                id: data['objects'][i].key,
                ref: data['objects'][i].fields.ref,
                approver_email: data['objects'][i].fields.approver_email,
                approver_id: data['objects'][i].fields.approver_id,
                caller_id: data['objects'][i].fields.caller_id,
                caller_id_friendlyname: data['objects'][i].fields.caller_id_friendlyname,
                caller_name: data['objects'][i].fields.caller_name,
                close_date: data['objects'][i].fields.close_date,
                description: data['objects'][i].fields.description,
                end_date: data['objects'][i].fields.end_date,
                last_update: data['objects'][i].fields.last_update,
                org_id: data['objects'][i].fields.org_id,
                org_name: data['objects'][i].fields.org_name,
                origin: data['objects'][i].fields.origin,
                request_type: data['objects'][i].fields.request_type,
                service_id: data['objects'][i].fields.service_id,
                service_name: data['objects'][i].fields.service_name,
                servicesubcategory_id: data['objects'][i].fields.servicesubcategory_id,
                servicesubcategory_name: data['objects'][i].fields.servicesubcategory_name,
                start_date: data['objects'][i].fields.start_date,
                status: data['objects'][i].fields.status,
                title: data['objects'][i].fields.title
              }
              self.solicitations.unshift(solicitation);
            }
            self.spliceArraySolicitations(user, self.loading);
        }
    });
  }

  public spliceArraySolicitations(user: { username: string, password: string, id: string, org_id: string }, loading: Loading): void{
    //this.loading.dismiss();
    this.new_solicitations = []; 
    this.solved_solicitations = [];

    this.solicitations.forEach(solicitation => {
      if(solicitation.status === "Novo" || solicitation.status === "Resolvido" || solicitation.status === "Pendente" || solicitation.status === "Atribuído"){
        this.new_solicitations.push(solicitation);
      }else if(solicitation.status === "Fechar"){
        this.solved_solicitations.push(solicitation);
      }
    });

    this.new_solicitations.reverse();
    this.solved_solicitations.reverse();

    this.serviceSubService.existsServices()
    .then((haveRows: boolean) => {
        if(!haveRows){
            loading.setContent("Buscando Serviços!");
            this.createService.createService(user, loading);
        }else{
          loading.dismiss();
        }
    })   
  }

  private showLoading(): Loading{
    let loading: Loading = this.loadingCtrl.create({
      content: 'Carregando Solicitações...'
    });

    loading.present();

    return loading;
  }

  onAddSolicitation(): void{
    this.navCtrl.push(AddSolicitationPage, {
      haveNewSolicitation: this.haveNewSolicitation
    });
  }

  onDetail(solicitation: Solicitation): void{
    this.navCtrl.push(DetailSolicitationPage, {
      solicitation: solicitation
    })
  }

  presentPopover(ev){
    let popover = this.popoverCtrl.create(PopoverPage, {
      contentEle: "Joao",
      textEle: "Neto"
    });

    popover.present({
      ev: ev
    });
  }

  toggleSearchbar(){
    this.showSearchBar = true;
  }

  checkBlur(){
    this.showSearchBar = false;
    this.searchTerm = '';

    this.searchArray = [];
  }

  filterItems(): void{
    //let searchTerm: string = event.target.value;
    this.searchArray = [];

    if(this.searchTerm){
      switch(this.view){
        case 'Novas':
        this.searchArray = this.new_solicitations.filter((item) => {
          console.log(item.title.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1);
          return item.ref.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1;
        });
          break;
        case 'Resolvidas':
        this.searchArray = this.solved_solicitations.filter((item) => {
          console.log(item.title.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1);
          return item.ref.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1;
        });
          break;
      }
    }
  }
}
