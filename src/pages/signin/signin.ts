import { Component } from '@angular/core';
import { NavController, Loading, LoadingController, AlertController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../providers/auth.service';
import { User } from '../../models/user.model';
import { PreferencesService } from '../../providers/preferences.service';
import { HomePage } from '../home/home';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { Network } from '@ionic-native/network';
import { ForgotpasswordPage } from '../../pages/forgotpassword/forgotpassword';

@Component({
  selector: 'page-signin',
  templateUrl: 'signin.html'
})
export class SigninPage {
  signinForm: FormGroup;
  user: User;
  data: string;

  constructor(
    public navCtrl: NavController, 
    public formBuilder: FormBuilder, 
    public authService: AuthService,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public preferences: PreferencesService,
    private toastCtrl: ToastController,
    private network: Network    
  ) {
        this.signinForm = this.formBuilder.group({
            username: ['', [ Validators.required, Validators.minLength(3) ]],
            password: ['', [ Validators.required, Validators.minLength(6) ]]
        });
  }

  ionViewDidEnter(){
    this.preferences.get()
    .then((user: User) => {
      if(user){
        console.log(user)
        this.navCtrl.setRoot(HomePage);
      }
    })
  }

  onSubmit(): void{
    let loading: Loading = this.showLoading();
    let formUser = this.signinForm.value;

    this.authService.createAuthUser({
        username: formUser.username,
        password: formUser.password
    }, this.navCtrl, loading, this.alertCtrl);
  }

  private showLoading(): Loading{
    let loading: Loading = this.loadingCtrl.create({
      content: 'Aguarde um momento...'
    });

    loading.present();

    return loading;
  }

  gotoForgotPwdPage() {
    this.navCtrl.push(ForgotpasswordPage);
  }
}